// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.



export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyBsil7xfKkcHBcFJ07Nz19HxjgF_WUxm5w',
    authDomain: 'proyectosg-sst.firebaseapp.com',
    databaseURL: 'https://proyectosg-sst.firebaseio.com',
    projectId: 'proyectosg-sst',
    storageBucket: 'proyectosg-sst.appspot.com',
    messagingSenderId: '673750028631',
    appId: '1:673750028631:web:a3448066eb29b262'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
