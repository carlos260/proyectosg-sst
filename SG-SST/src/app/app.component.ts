import { Component } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { PermissionService } from './services/permission.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'SG-SST';
  constructor(db: AngularFirestore) {
  }
}