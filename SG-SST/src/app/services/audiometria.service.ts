import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { audiometria } from '../models/Audiometria.model';

@Injectable({
    providedIn: 'root'
})
export class AudiometriaService {
    private Url = 'https://proyectosg-sst.firebaseio.com';
    constructor(private http: HttpClient) { }

    getAudiometria(id: string) {
        return this.http.get(`${this.Url}/audiometrias/${id}.json`);
    }

    crearAudiometria(audiometria: audiometria) {
        return this.http.post(`${this.Url}/audiometrias.json`, audiometria)
            .pipe(
                map((respt: any) => {
                    audiometria.id = respt.name;
                    return audiometria;
                })
            );
    }

    actualizarAdiometria(audiometria: audiometria) {
        const audiometriatemp = {
            ...audiometria
        };

        delete audiometriatemp.id;
        return this.http.put(`${this.Url}/audiometrias/${audiometria.id}.json`, audiometriatemp);
    }

    getAudiometrias() {
        return this.http.get(`${this.Url}/audiometrias.json`)
            .pipe(
                map(this.CrearArreglo)
            );
    }

    EliminarAudiometria(id: string) {
        return this.http.delete(`${this.Url}/audiometrias/${id}.json`);
    }

    private CrearArreglo(audiometriaObj: object) {
        const audiometrias: audiometria[] = [];

        if (audiometriaObj === null) {
            return [];
        } else {
            Object.keys(audiometriaObj).forEach(key => {
                const audiometriaa: audiometria = audiometriaObj[key];
                audiometriaa.id = key;

                audiometrias.push(audiometriaa);
            });
        }
        return audiometrias;
    }
}