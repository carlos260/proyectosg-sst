import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { PermisoInterface } from '../models/permiso.model';

@Injectable({
  providedIn: 'root'
})
export class PermissionService {

  private permisosCollection: AngularFirestoreCollection<PermisoInterface>;

  constructor( private afs: AngularFirestore ) {}

  public getPermisos(): Observable<PermisoInterface[]> {
    this.permisosCollection = this.afs.collection<PermisoInterface>('permisos');
    return this.permisosCollection.valueChanges();
  }

  public agregarPermiso( permiso: PermisoInterface ): Promise<void> {
    const id = this.afs.createId();
    const objectTMP: PermisoInterface = {
      id,
      nombre: permiso.nombre,
      permiso: permiso.permiso
    };
    return this.permisosCollection.doc(id).set(objectTMP);
  }

  public removerPermiso(permiso: PermisoInterface): void {
    this.permisosCollection.doc(permiso.id).delete();
  }

  public actualizarPermiso(permiso: PermisoInterface): Promise<void> {
    return this.permisosCollection.doc(permiso.id).update(permiso);
  }

  public traerPermiso( id: string ) {
    return this.permisosCollection.doc(id).valueChanges();
  }
}


