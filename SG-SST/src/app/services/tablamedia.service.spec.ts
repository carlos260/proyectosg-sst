import { TestBed } from '@angular/core/testing';

import { TablamediaService } from './tablamedia.service';

describe('TablamediaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TablamediaService = TestBed.get(TablamediaService);
    expect(service).toBeTruthy();
  });
});
