import { Injectable } from '@angular/core';

import { RolModel, RolInterface } from '../models/Rol.model';
import { AngularFirestoreCollection, AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class RolService {
  private rolercollection: AngularFirestoreCollection<RolInterface>;

  constructor( private afs: AngularFirestore ) {}

  public traerRoles(): Observable<RolInterface[]> {
    this.rolercollection = this.afs.collection<RolInterface>('roles');
    return this.rolercollection.valueChanges();
  }

  public agregarRol( rol: RolInterface ): Promise<void> {
    const id = this.afs.createId();
    const objectTMP: RolInterface = {
      id,
      nombre: rol.nombre,
      estado: rol.estado,
      permisos: rol.permisos
    };
    return this.rolercollection.doc(id).set(objectTMP);
  }

  public removerRoles(rol: RolInterface): void {
    this.rolercollection.doc(rol.id).delete();
  }

  public actualizarRoles(rol: RolInterface): Promise<void> {
    return this.rolercollection.doc(rol.id).update(rol);
  }

  public traerRol( id: string ) {
    return this.rolercollection.doc(id).valueChanges();
  }
}
