import { TestBed } from '@angular/core/testing';

import { CondicionesSaludService } from './condicionesSalud.service';

describe('CondicionesSaludService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CondicionesSaludService = TestBed.get(CondicionesSaludService);
    expect(service).toBeTruthy();
  });
});