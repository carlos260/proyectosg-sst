import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { MicrosoftService } from './microsoft.service';
import { tap, map, take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class GuardService implements CanActivate{
  constructor(private auth: MicrosoftService,
              private router: Router) { }


  canActivate(next, state): Observable<boolean>{
      return this.auth.user$.pipe(
        take(1),
        map(user => !! user),
        tap(loggedin => {
          if(!loggedin){
            console.log('acces denied');
            this.router.navigate(['/Login']);
          }
        }
        )
      );
  }
  

  
}
