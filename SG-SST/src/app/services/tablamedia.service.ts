import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { RolPermisoModel } from '../models/rolp.model';
import { RolService } from './rol.service';
import { RolModel } from '../models/Rol.model';
import { PermisoModel } from '../models/permiso.model';

@Injectable({
  providedIn: 'root'
})
export class TablamediaService {

  constructor( private http: HttpClient,
               private rol: RolService ) { }

  private url = `https://proyectosg-sst.firebaseio.com`;

  getRolesP() {
    return this.http.get(`${ this.url }/RolPermission.json`).pipe(
      map ( this.crearArreglo )
    );

  }

  crearArreglo( rolespObj: object ) {
      const rolesp: RolPermisoModel[] = [];

      if ( rolespObj === null ) {
          return  [];
      } else {
        Object.keys (rolespObj ).forEach(key => {
          const rolp: RolPermisoModel  = rolespObj[key];
          rolp.id = key;
          rolesp.push( rolp );
          console.log( rolp );
        });
      }
      return rolesp;
  }

  actualizarRoles() {
  }

}
