import { Injectable } from '@angular/core';
import { AngularFirestoreCollection, AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { EmpresaInterface } from '../models/Empresa.model';

@Injectable({
  providedIn: 'root'
})
export class EmpresaService {
  private rolercollection: AngularFirestoreCollection<EmpresaInterface>;

  constructor( private afs: AngularFirestore) { }


  public traerEmpresas(): Observable<EmpresaInterface[]> {
    this.rolercollection = this.afs.collection<EmpresaInterface>('empresas');
    return this.rolercollection.valueChanges();
  }

  public agregarEmpresa( empresa: EmpresaInterface ): Promise<void> {
    const id = this.afs.createId();
    const objectTMP: EmpresaInterface = {
      id,
      nombre: empresa.nombre,
      empresaVinculada: empresa.empresaVinculada      
    };
    return this.rolercollection.doc(id).set(objectTMP);
  }

  public removerEmpresa(empresa: EmpresaInterface): void {
    this.rolercollection.doc(empresa.id).delete();
  }

  public actualizarEmpresa(empresa: EmpresaInterface): Promise<void> {
    return this.rolercollection.doc(empresa.id).update(empresa);
  }

  public traerEmpresa( id: string ) {
    return this.rolercollection.doc(id).valueChanges();
  }
}
