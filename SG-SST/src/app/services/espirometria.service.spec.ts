import { TestBed } from '@angular/core/testing';

import { EspirometriaService } from './espirometria.service';

describe('EspirometriaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EspirometriaService = TestBed.get(EspirometriaService);
    expect(service).toBeTruthy();
  });
});