import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { condicionesSaludModel } from '../models/condicionesSalud.model';

@Injectable({
  providedIn: 'root'
})
export class CondicionesSaludService {
  private Url = 'https://proyectosg-sst.firebaseio.com';
  constructor(private http: HttpClient) { }

  getCondicioneSalud(id: string) {
      return this.http.get(`${this.Url}/condicionesSalud/${id}.json`);
  }

  crearCondicionesSalud(condicionesSalud: condicionesSaludModel) {
      return this.http.post(`${this.Url}/condicionesSalud.json`, condicionesSalud)
          .pipe(
              map((respt: any) => {
                condicionesSalud.id = respt.name;
                  return condicionesSalud;
              })
          );
  }

  actualizarCondicionesSalud(condicionesSalud: condicionesSaludModel) {
      const condicionesSaludtemp = {
          ...condicionesSalud
      };

      delete condicionesSaludtemp.id;
      return this.http.put(`${this.Url}/condicionesSalud/${condicionesSalud.id}.json`, condicionesSaludtemp);
  }

  getCondicionesSalud() {
      return this.http.get(`${this.Url}/condicionesSalud.json`)
          .pipe(
              map(this.CrearArreglo)
          );
  }

  EliminarCondicionesSalud(id: string) {
      return this.http.delete(`${this.Url}/condicionesSalud/${id}.json`);
  }

  private CrearArreglo(condicionesSaludObj: object) {
      const condicionesSalud: condicionesSaludModel[] = [];

      if (condicionesSaludObj === null) {
          return [];
      } else {
          Object.keys(condicionesSaludObj).forEach(key => {
              const condicionSalud: condicionesSaludModel = condicionesSaludObj[key];
              condicionSalud.id = key;

              condicionesSalud.push(condicionSalud);
          });
      }
      return  condicionesSalud;
  }
}