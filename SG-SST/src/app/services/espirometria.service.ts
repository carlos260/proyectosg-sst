import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { EspirometriaModel } from '../models/espirometria.model';

@Injectable({
  providedIn: 'root'
})
export class EspirometriaService {
  private Url = 'https://proyectosg-sst.firebaseio.com';
  constructor(private http: HttpClient) { }

  getEspirometria(id: string) {
      return this.http.get(`${this.Url}/espirometrias/${id}.json`);
  }

  crearEspirometria(espirometria: EspirometriaModel) {
      return this.http.post(`${this.Url}/espirometrias.json`, espirometria)
          .pipe(
              map((respt: any) => {
                  espirometria.id = respt.name;
                  return espirometria;
              })
          );
  }

  actualizarEspirometria(espirometria: EspirometriaModel) {

      const espirometriatemp = {
          ...espirometria
      };

      delete espirometriatemp.id;
      return this.http.put(`${this.Url}/espirometrias/${espirometria.id}.json`, espirometriatemp);
  }

  getEspirometrias() {
      return this.http.get(`${this.Url}/espirometrias.json`)
          .pipe(
              map(this.CrearArreglo)
          );
  }

  EliminarEspirometria(id: string) {
      return this.http.delete(`${this.Url}/espirometrias/${id}.json`);
  }

  private CrearArreglo(espirometriasObj: object) {
      const espirometrias: EspirometriaModel[] = [];

      if (espirometriasObj === null) {
          return [];
      } else {
          Object.keys(espirometriasObj).forEach(key => {
              const espirometria: EspirometriaModel = espirometriasObj[key];
              espirometria.id = key;

              espirometrias.push(espirometria);
          });
      }
      return espirometrias;
  }
}
