import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { TamizajeModel } from '../models/tamizaje.model';

@Injectable({
    providedIn: 'root'
})

export class TamizajeService {
    private Url = 'https://proyectosg-sst.firebaseio.com';
    constructor(private http: HttpClient) { }

    getTamizaje(id: string) {
        return this.http.get(`${this.Url}/tamizajes/${id}.json`);
    }

    crearTamizaje(tamizaje: TamizajeModel) {
        return this.http.post(`${this.Url}/tamizajes.json`, tamizaje)
            .pipe(
                map((respt: any) => {
                    tamizaje.id = respt.name;
                    return tamizaje;
                })
            );
    }

    actualizarTamizaje(tamizaje: TamizajeModel) {

        const tamizajetemp = {
            ...tamizaje
        };

        delete tamizajetemp.id;
        return this.http.put(`${this.Url}/tamizajes/${tamizaje.id}.json`, tamizajetemp);
    }

    getTamizajes() {
        return this.http.get(`${this.Url}/tamizajes.json`)
            .pipe(
                map(this.CrearArreglo)
            );
    }

    EliminarTamizaje(id: string) {
        return this.http.delete(`${this.Url}/tamizajes/${id}.json`);
    }

    private CrearArreglo(tamizajesObj: object) {
        const tamizajes: TamizajeModel[] = [];

        if (tamizajesObj === null) {
            return [];
        } else {
            Object.keys(tamizajesObj).forEach(key => {
                const tamizaje: TamizajeModel = tamizajesObj[key];
                tamizaje.id = key;

                tamizajes.push(tamizaje);
            });
        }
        return tamizajes;
    }


}