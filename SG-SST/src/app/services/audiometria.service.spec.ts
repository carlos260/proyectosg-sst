import { TestBed } from '@angular/core/testing';

import { AudiometriaService } from './audiometria.service';

describe('AudiometriaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AudiometriaService = TestBed.get(AudiometriaService);
    expect(service).toBeTruthy();
  });
});