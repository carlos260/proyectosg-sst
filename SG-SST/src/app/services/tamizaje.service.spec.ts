import { TestBed } from '@angular/core/testing';

import { TamizajeService } from './tamizaje.service';

describe('TamizajeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TamizajeService = TestBed.get(TamizajeService);
    expect(service).toBeTruthy();
  });
});