import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { switchMap, merge } from 'rxjs/operators';
import { userInterface } from '../models/user.model';
import { auth } from 'firebase';
//npm install --save firebase
@Injectable({
  providedIn: 'root'
})
export class MicrosoftService {

  user$: Observable<userInterface>;

  constructor(
    private aFauth: AngularFireAuth,
    private afs: AngularFirestore,
    private router: Router
  ) { 
    this.user$ = this.aFauth.authState.pipe(
      switchMap(user => {
        if ( user ){
            return this.afs.doc<userInterface>(`users/${user.uid}`).valueChanges();
        }  else {
          return of(null);
        }
      })
    );
  }

  async MicrosotLogin(){
    const provider = new auth.OAuthProvider('microsoft.com');
    const credential = await this.aFauth.auth.signInWithPopup(provider);
    console.log(credential);
    this.router.navigate(['/Home']);
    return this.updateUserData( credential.user );
    
  }

  async singOut(){
    await this.aFauth.auth.signOut();
    return this.router.navigate(['/']);
  }

  public updateUserData( user ){
    const userRef: AngularFirestoreDocument<userInterface> = this.afs.doc(`users/${user.uid}`);

    const data = {
      id: user.uid,
      nombre: user.displayName,
      apellido: user.apellido,
      email: user.email,
      cedula: user.cedula,
      fechaed: user.fechaed = null,
      rol: user.rol,
      empresaAsociada: user.empresaAsociada

    };

    return userRef.set(data, {merge: true});
  }
}
