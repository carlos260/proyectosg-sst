import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { ReporteModel } from '../models/reporte.model';


@Injectable({
  providedIn: 'root'
})
export class ReporteService {
  private Url = 'https://proyectosg-sst.firebaseio.com';

  constructor(private http: HttpClient) { }

  getReporte(id: string) {
    return this.http.get(`${this.Url}/reportes/${id}.json`);
  }

  crearReporte(reporte: ReporteModel) {
    return this.http.post(`${this.Url}/reportes.json`, reporte)
      .pipe(
        map((respt: any) => {
          reporte.id = respt.name;
          return reporte;
        })
      );
  }

  actualizarReporte(reporte: ReporteModel) {

    const reportetemp = {
      ...reporte
    };

    delete reportetemp.id;
    return this.http.put(`${this.Url}/reportes/${reporte.id}.json`, reportetemp);
  }

  getReportes() {
    return this.http.get(`${this.Url}/reportes.json`)
      .pipe(
        map(this.CrearArreglo)
      );
  }

  EliminarReporte(id: string) {
    return this.http.delete(`${this.Url}/reportes/${id}.json`);
  }

  private CrearArreglo(reportesObj: object) {

    const reportes: ReporteModel[] = [];

    if (reportesObj === null) {
      return [];
    } else {
      Object.keys(reportesObj).forEach(key => {
        const reporte: ReporteModel = reportesObj[key];
        reporte.id = key;

        reportes.push(reporte);
      });
    }

    return reportes;

  }
}