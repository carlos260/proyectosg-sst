import { Injectable } from '@angular/core';
import { AngularFirestoreCollection, AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { userInterface } from '../models/user.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  
  private userCollection: AngularFirestoreCollection<userInterface>;

  constructor( private afs: AngularFirestore ) { 
    
  }

  /*traerDecisionesMedicas(idUser: string): Observable<userInterface[]> {
      //this.userCollection = this.afs.collection<any>('decisionesMedicas', ref => ref.where('idUser','==',idUser));
      return this.userCollection.valueChanges();
  }*/

  updateUser(user: userInterface): Promise<void>{
    this.userCollection = this.afs.collection<userInterface>('users');
    return this.userCollection.doc(user.id).update(user);
  }

  traerUsuarios(){
    this.userCollection = this.afs.collection<userInterface>('users');
    return this.userCollection.valueChanges();
  }
}
