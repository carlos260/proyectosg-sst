import { Component, OnInit } from '@angular/core';
import { RolService } from '../../services/rol.service';
import { RolInterface, RolModel } from '../../models/Rol.model';
import { PermissionService } from '../../services/permission.service';
import { PermisoModel } from '../../models/permiso.model';

@Component({
  selector: 'app-roles',
  templateUrl: './roles.component.html',
  styleUrls: ['./roles.component.css']
})
export class RolesComponent implements OnInit {

  roles: RolModel[] = [];
  permisos: any[] = [];
  nombre: string;
  cargando = true;

  constructor( private rs: RolService,
               private ps: PermissionService) { }

  ngOnInit() {
    this.cargando = true;
    this.rs.traerRoles().subscribe(
      (resp) => {
        console.log(resp);
        this.roles = resp;
        this.cargando = false;
      }
    );
  }

  removerRol( rol: RolInterface): void {
    this.rs.removerRoles(rol);
  }

}
