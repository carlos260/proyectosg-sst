import { Component, OnInit } from '@angular/core';
import { MicrosoftService } from '../../services/microsoft.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor(public auth: MicrosoftService) { }

  ngOnInit() {
    console.log('Ingresando al navbar');
  }

}
