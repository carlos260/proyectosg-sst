import { Component, OnInit } from '@angular/core';
import { AuthFirebaseService } from '../../providers/auth/auth-firebase.service';
import { MicrosoftService } from '../../services/microsoft.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor( public auth: MicrosoftService) { }

  ngOnInit() {
  }

}
