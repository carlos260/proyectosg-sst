import { Component, OnInit } from '@angular/core';
import { auth } from 'firebase';
import { MicrosoftService } from '../../services/microsoft.service';
import { userInterface } from '../../models/user.model';
import { TamizajeService } from '../../services/tamizaje.service';
import { UserService } from 'src/app/services/user.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  users: userInterface[];

  constructor(public auth: MicrosoftService,
    private us: UserService) { }
  

  ngOnInit() {
    this.us.traerUsuarios().subscribe(
      (resp) => {
        this.users = resp;
      }
    )
  }

}
