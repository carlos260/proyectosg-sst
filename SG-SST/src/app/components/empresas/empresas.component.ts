import { Component, OnInit } from '@angular/core';
import { EmpresaService } from '../../services/empresa.service';
import { EmpresaInterface } from '../../models/Empresa.model';


@Component({
  selector: 'app-empresas',
  templateUrl: './empresas.component.html',
  styleUrls: ['./empresas.component.css']
})
export class EmpresasComponent implements OnInit {
  Empresas: EmpresaInterface[] = [];
  cargando = true;
  constructor( private emp: EmpresaService) { }

  ngOnInit() {
    this.emp.traerEmpresas().subscribe(
      (resp) => {
        this.Empresas = resp;
        this.cargando = false;
      }
    )
  }

  removerEmpresa(empresa: EmpresaInterface): void{
    this.emp.removerEmpresa(empresa);
  }

}
