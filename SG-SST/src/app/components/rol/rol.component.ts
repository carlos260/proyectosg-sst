import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { PermisoModel } from '../../models/permiso.model';
import { RolService } from '../../services/rol.service';
import { PermissionService } from '../../services/permission.service';
import { RolModel, RolInterface } from '../../models/Rol.model';
import { ActivatedRoute } from '@angular/router';
import {Location} from '@angular/common';

@Component({
  selector: 'app-rol',
  templateUrl: './rol.component.html',
  styleUrls: ['./rol.component.css']
})
export class RolComponent implements OnInit {

  permisos: PermisoModel[] = [];
  rol: RolModel = new RolModel();
  public id = '';

  constructor(private rs: RolService,
              private ps: PermissionService,
              private route: ActivatedRoute,
              private location: Location
              ) {
                this.rol.permisos = [];
               }

  ngOnInit() {
    this.id =  this.route.snapshot.paramMap.get('id');
    this.ps.getPermisos().subscribe(
      (permisos) => {
        this.permisos = permisos;
      }
    );
    if ( this.id !== 'nuevo' ) {
      this.rs.traerRol( this.id ).subscribe(
        (resp: RolInterface) => {
          this.rol = resp;
        }
      );
    }
  }
  validacion( form: NgForm ) {
    console.log( this.rol );
    if ( form.invalid ) {
      console.log('formulario no valido');
      return;
    }
    if (this.id !== 'nuevo') {
      this.editar();
    } else {
      this.guardar();
    }
  }

  public selectPermision(index: number): void {
    if (this.rol.permisos.indexOf(this.permisos[index].nombre) < 0) {
      this.rol.permisos.push(this.permisos[index].nombre);
      console.log(index);
    } else {
      this.rol.permisos.splice( this.rol.permisos.indexOf(this.permisos[index].nombre) , 1);
      console.log('seleccionado');
    }
    console.log('this.rol.permisos' , this.rol.permisos);
  }

  public guardar(): void {
    this.rs.agregarRol( this.rol ).then(
      () => {
      this.location.back();
      }
    );
  }

  public editar(): void {
    this.rs.actualizarRoles( this.rol ).then(
      () => {
      this.location.back();
      }
    );
  }
}
