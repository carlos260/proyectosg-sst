import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { userInterface, userModel } from '../../models/user.model';
import { MicrosoftService } from '../../services/microsoft.service';
import { EmpresaInterface } from '../../models/Empresa.model';
import { RolInterface } from '../../models/Rol.model';
import { RolService } from '../../services/rol.service';
import { EmpresaService } from '../../services/empresa.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  
  user : userInterface = new userModel();
  empresas : EmpresaInterface[];  
  roles: RolInterface[];

  constructor( private us: UserService ,
    private lg: MicrosoftService,
    private rol: RolService,
    private empre: EmpresaService) { }

  ngOnInit() {
      this.lg.user$.subscribe(
        (resp) => {
          console.log(resp);
          this.user = resp;
        }        
      )

      this.rol.traerRoles().subscribe(
        (resp) => {
          this.roles = resp;
        }
      )

      this.empre.traerEmpresas().subscribe(
        (resp) => {
          this.empresas = resp;
        }
      )
  }
  
  update (form : NgForm): void{
    console.log(this.user);
    this.us.updateUser(this.user);
  }

}
