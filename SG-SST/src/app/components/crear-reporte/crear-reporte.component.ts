import { Component, OnInit } from '@angular/core';
import { ReporteModel } from '../../models/reporte.model';
import { NgForm } from '@angular/forms';
import { ReporteService } from '../../services/reporte.service';
import { ActivatedRoute } from '@angular/router';
import swal from 'sweetalert2';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-crear-reporte',
  templateUrl: './crear-reporte.component.html',
  styleUrls: ['./crear-reporte.component.css']
})
export class CrearReporteComponent implements OnInit {

  reporte: ReporteModel = new ReporteModel();

  constructor( private ReporteService: ReporteService,
               private route: ActivatedRoute ) { }

  ngOnInit() {

    const id = this.route.snapshot.paramMap.get('id');

    if ( id !== 'nuevo') {
      this.ReporteService.getReporte( id )
          .subscribe( (resp: ReporteModel) => {
              console.log(resp);
              this.reporte = resp;
              this.reporte.id = id;
          });
    }
  }

  
  guardar( form: NgForm ) {

    if ( form.invalid ) {
      console.log('formulario no valido');
      return;
    }

    swal.fire({
      title: 'Espere',
      text: 'S e esta guardando su informacion',
      type: 'info',
      allowOutsideClick: false
    });
    swal.showLoading();


    let peticion: Observable <any>;

    if ( this.reporte.id ) {
      peticion = this.ReporteService.actualizarReporte( this.reporte );
    } else {
      peticion = this.ReporteService.crearReporte( this.reporte);
    }

    peticion.subscribe(resp => {
      swal.fire({
        title: this.reporte.nombre,
        text: 'Su informacion se a guardado correctamente',
        type: 'success'
      });
    });
  }

}
