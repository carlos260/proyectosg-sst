import { Component, OnInit } from '@angular/core';
import { EmpresaService } from '../../services/empresa.service';
import { EmpresaInterface, EmpresaModel } from '../../models/Empresa.model';
import { NgForm } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import {Location} from '@angular/common';

@Component({
  selector: 'app-empresa',
  templateUrl: './empresa.component.html',
  styleUrls: ['./empresa.component.css']
})
export class EmpresaComponent implements OnInit {

  Empresas: EmpresaInterface[] = [];
  empre: EmpresaModel = new EmpresaModel();
  public id = '';

  constructor( private emp: EmpresaService,
    private route: ActivatedRoute,
    private location: Location) { }

  ngOnInit() {
    this.id =  this.route.snapshot.paramMap.get('id');
    this.emp.traerEmpresas().subscribe(
      (resp) => {
        this.Empresas = resp;
      }
    )
  }

  public validar(f: NgForm){
    if (this.id !== 'nuevo') {
      this.editar();
    } else {
      this.guardar();
    }
  }

  public guardar(): void{
    this.emp.agregarEmpresa(this.empre).then(
      () => {
        this.location.back();
      }
    )
  }

  public editar(): void{
    this.emp.actualizarEmpresa(this.empre).then(
      () => {
        this.location.back();
      }
    )
  }

}
