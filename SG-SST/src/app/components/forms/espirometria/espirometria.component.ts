import { Component, OnInit } from '@angular/core';
import { EspirometriaModel } from '../../../models/espirometria.model';
import { EspirometriaService } from '../../../services/espirometria.service';
import { ActivatedRoute } from '@angular/router';
import swal from 'sweetalert2';
import { Observable } from 'rxjs';

@Component({
  selector: 'espirometriaForm',
  templateUrl: './espirometria.component.html',
  styleUrls: ['./espirometria.component.css']
})
export class EspirometriaComponent implements OnInit {

  espirometria: EspirometriaModel = new EspirometriaModel();
  model: any;
  constructor(private EspirometriaService: EspirometriaService, private route: ActivatedRoute) { }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    const idd = "nuevo";
    if (idd !== 'nuevo') {
      this.EspirometriaService.getEspirometria(id)
        .subscribe((resp: EspirometriaModel) => {
          this.espirometria = resp;
          this.espirometria.id = id;
        });
    }
  }

  opciones = [
    { id: "si", label: "Si" },
    { id: "no", label: "No" }
  ]
  evaluacion= [
    { id: "Normal", label: "Normal" },
    { id: "Obstructiva", label: "Obstructiva" },
    { id: "Restrictiva", label: "Restrictiva" },
    { id: "Mixta", label: "Mixta" },
    { id: "Leve", label: "Leve" },
    { id: "Moderada", label: "Moderada" },
    { id: "Severa", label: "Severa" },
    { id: "No evaluable", label: "No evaluable" }
  ]

  guardar(model: EspirometriaModel, isValid: boolean) {
  
    swal.fire({
      title: 'Espere',
      text: 'Se esta guardando su informacion',
      type: 'info',
      allowOutsideClick: false
    });
    swal.showLoading();


    let peticion: Observable<any>;

    if (this.espirometria.id) {
      peticion = this.EspirometriaService.actualizarEspirometria(this.espirometria);
    } else {
      peticion = this.EspirometriaService.crearEspirometria(this.espirometria);
    }

    peticion.subscribe(resp => {
      swal.fire({
        title: this.espirometria.nombre,
        text: 'Su informacion se ha guardado correctamente',
        type: 'success'
      });
    });
  }

}
