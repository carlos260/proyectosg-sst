import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CondicionesSaludComponent } from './condiciones-salud.component';

describe('CondicionesSaludComponent', () => {
  let component: CondicionesSaludComponent;
  let fixture: ComponentFixture<CondicionesSaludComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CondicionesSaludComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CondicionesSaludComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
