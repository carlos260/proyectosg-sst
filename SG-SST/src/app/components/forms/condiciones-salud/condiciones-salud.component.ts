import { Component, OnInit } from '@angular/core';
import { condicionesSaludModel } from '../../../models/condicionesSalud.model';
import { CondicionesSaludService } from '../../../services/condicionesSalud.service';
import { ActivatedRoute } from '@angular/router';
import swal from 'sweetalert2';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-condiciones-salud',
  templateUrl: './condiciones-salud.component.html',
  styleUrls: ['./condiciones-salud.component.css']
})
export class CondicionesSaludComponent implements OnInit {

  condicionesSalud: condicionesSaludModel = new condicionesSaludModel();
  constructor(private CondicionesSaludService: CondicionesSaludService, private route: ActivatedRoute) { }


  estado = [
    { id: "soltero", label: "Soltero" },
    { id: 'casado', label: 'Casado' },
    { id: 'separado', label: 'Separado' },
    { id: 'viudo', label: 'Viudo' },
    { id: 'Unión libre', label: 'Unión libre' }
  ]

  nivelAcademico = [
    { id: 'primaria', label: 'Primaria' },
    { id: 'secundaria', label: 'Secundaria' },
    { id: 'tecnologo', label: 'Tecnólogo' },
    { id: 'universitario', label: 'Universitario' },
    { id: 'postgrado', label: 'Postgrado' },
  ]
  vinculacion = [
    { id: 'planta', label: 'Planta' },
    { id: 'contrato', label: 'Contrato' }
  ]

  turno = [
    { id: 'diurno', label: 'Diurno' },
    { id: 'nocturno', label: 'Nocturno' },
    { id: 'mixto', label: 'Mixto' },
    { id: 'disponibilidad', label: 'Disponibilidad' }
  ]

  funciones = [
    { id: 'administrativas', label: 'Administrativas' },
    { id: 'custodia y vigilancia', label: 'Custodia y Vigilancia' },
    { id: 'mixtas', label: 'Mixtas' },
    { id: 'otras', label: 'Otras' }
  ]
  antecedentesF = [
    { id: 'no', label: 'No' },
    { id: 'si', label: 'Si' }
  ]

  infeccionesInfancia = [
    { id: 'no', label: 'No' },
    { id: 'si', label: 'Si' },
    { id: 'ns', label: 'NS' }
  ]

  antecedentesT = [
    { id: 'anteriormente', label: 'Anteriormente' },
    { id: 'actualmete', label: 'Actualmete' },
    { id: 'diario', label: 'Diario' },
    { id: 'semanalmente', label: 'Semanalmente' },
    { id: 'quincenal', label: 'Quincenal' },
    { id: 'ocasional', label: 'Ocasional' }
  ]

  elementos = [
    { id: 'necesita', label: 'necesita' },
    { id: 'tiene', label: 'Tiene' },
    { id: 'utiliza', label: 'Utiliza' }
  ]

  ngOnInit() {

    const id = this.route.snapshot.paramMap.get('id');
    console.log(id + 'iddddd');
    const idd = "nuevo";
    if (idd !== 'nuevo') {
      console.log("ENTRA AQUIIII");
      this.CondicionesSaludService.getCondicioneSalud(id)
        .subscribe((resp: condicionesSaludModel) => {
          this.condicionesSalud = resp;
          this.condicionesSalud.id = id;
        });
    }
  }



  guardar(model: condicionesSaludModel, isValid: boolean) {

    swal.fire({
      title: 'Espere',
      text: 'Se esta guardando su informacion',
      type: 'info',
      allowOutsideClick: false
    });
    swal.showLoading();


    let peticion: Observable<any>;

    if (this.condicionesSalud.id) {
      peticion = this.CondicionesSaludService.actualizarCondicionesSalud(this.condicionesSalud);
    } else {
      peticion = this.CondicionesSaludService.crearCondicionesSalud(this.condicionesSalud);
    }

    peticion.subscribe(resp => {
      swal.fire({
        title: this.condicionesSalud.nombre,
        text: 'Su informacion se ha guardado correctamente',
        type: 'success'
      });
    });
  }

}
