import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AudiometriaOcupacionalComponent } from './audiometria-ocupacional.component';

describe('AudiometriaOcupacionalComponent', () => {
  let component: AudiometriaOcupacionalComponent;
  let fixture: ComponentFixture<AudiometriaOcupacionalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AudiometriaOcupacionalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AudiometriaOcupacionalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
