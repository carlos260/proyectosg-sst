import { Component, OnInit } from '@angular/core';
import { audiometria } from '../../../models/Audiometria.model';
import { AudiometriaService } from '../../../services/audiometria.service';
import { ActivatedRoute } from '@angular/router';
import swal from 'sweetalert2';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-audiometria-ocupacional',
  templateUrl: './audiometria-ocupacional.component.html',
  styleUrls: ['./audiometria-ocupacional.component.css']
})
export class AudiometriaOcupacionalComponent implements OnInit {

  audiometria: audiometria = new audiometria();
  constructor(private AudiometriaService: AudiometriaService, private route: ActivatedRoute) {

  }

  ngOnInit() {

    const id = this.route.snapshot.paramMap.get('id');
    const idd = "nuevo";
    if (idd !== 'nuevo') {
      this.AudiometriaService.getAudiometria(id)
        .subscribe((resp: audiometria) => {
          this.audiometria = resp;
          this.audiometria.id = id;
        });
    }
  }


  antecedentes = [
    { id: 'no', label: 'No' },
    { id: 'si', label: 'Si' }
  ]

  otoscopia = [
    { id: 'normal', label: 'Normal' },
    { id: 'taponp', label: 'Tapón Parcial de Cerumen' },
    { id: 'tapot', label: 'Tapón Total de Cerumen' }
  ]

  timpanica = [
    { id: 'normal', label: 'Nomal' },
    { id: 'alterada', label: 'Alterada' }
  ]

  audiom = [
    { id: 'normal', label: '< 20 dB Audición Normal' },
    { id: 'leve', label: '21 - 40 Hipoacusia Leve' },
    { id: 'moderada', label: '41 - 70 Hipoacusia Moderada' },
    { id: 'severa', label: '71 - 90 Hipoacusia Severa' },
    { id: 'profunda', label: '> 90 Hipoacusia Profunda' }
  ]

  result = [
    { id: 'normal', label: 'Normal' },
    { id: 'anormal', label: 'Anormal' }
  ]
  guardar(model: audiometria, isValid: boolean) {

    swal.fire({
      title: 'Espere',
      text: 'Se esta guardando su informacion',
      type: 'info',
      allowOutsideClick: false
    });
    swal.showLoading();


    let peticion: Observable<any>;

    if (this.audiometria.id) {
      peticion = this.AudiometriaService.actualizarAdiometria(this.audiometria);
    } else {
      peticion = this.AudiometriaService.crearAudiometria(this.audiometria);
    }

    peticion.subscribe(resp => {
      swal.fire({
        title: this.audiometria.nombres,
        text: 'Su informacion se ha guardado correctamente',
        type: 'success'
      });
    });
  }



}
