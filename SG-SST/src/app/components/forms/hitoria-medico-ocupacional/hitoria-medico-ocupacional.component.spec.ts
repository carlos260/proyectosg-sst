import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HitoriaMedicoOcupacionalComponent } from './hitoria-medico-ocupacional.component';

describe('HitoriaMedicoOcupacionalComponent', () => {
  let component: HitoriaMedicoOcupacionalComponent;
  let fixture: ComponentFixture<HitoriaMedicoOcupacionalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HitoriaMedicoOcupacionalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HitoriaMedicoOcupacionalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
