import { Component, OnInit, ViewChild } from '@angular/core';
import { TamizajeModel } from '../../../models/tamizaje.model';
import { NgForm, FormBuilder, FormGroup } from '@angular/forms';
import { TamizajeService } from '../../../services/tamizaje.service';
import { ActivatedRoute } from '@angular/router';
import swal from 'sweetalert2';
import { Observable, Subject, merge } from 'rxjs';
import { startWith, debounceTime, distinctUntilChanged, filter, map, isEmpty } from 'rxjs/operators';
import { NgbTypeahead } from '@ng-bootstrap/ng-bootstrap';
import {formatDate} from '@angular/common';

const states = ['Bogotá', 'Medellín', 'Cali', 'Barranquilla', 'Cartagena', 'Soeledad', 'Cúcuta',
  'Soacha', 'Ibague', 'Bucaramanga', 'Villavicencio', 'Santa Marta', 'Bello',
  'Valledupar', 'Pereira', 'Buenaventura', 'Pasto', 'Manizalez', 'Monetría', 'Neiva', 'Palmira', 'Armenia', 'Rioacha',
  'Sincelejo', 'Popayan', 'Itagüi', 'Floridablanca', 'Envigado', 'Tuluá', 'Tumaco', 'Dosquebradas',
  'Tunja', 'Apartadó', 'Guirón'];

const profesional = ['Andres Lopez', 'Arturo Díaz', 'Cesar Cstro', 'Diego Salamanca', 'Jhon Daza', 'Ana María Gozazlez', 'Leidy Cardenas',
  'Daniel Chacon', 'Laura Rodriguez', 'Dayan Bastias', 'Carlos Londoño', 'Sebastian Martinez', 'Martin Cortes', 'David Diaz',
];

const establecimiento = ['Salud Total', 'Coomeva', 'Compensar', 'Sanitas', 'Virrey Solis', 'Capital Salud',
  'Sura', 'Alianza salud', 'Cafe Salus', 'Confenalco', 'Humana Vivir', 'Famisanar', 'Cruz Blanca', 'Nueva EPS'];
@Component({
  selector: 'app-tamizaje-cardiovascular',
  templateUrl: './tamizaje-cardiovascular.component.html',
  styleUrls: ['./tamizaje-cardiovascular.component.css']
})
export class TamizajeCardiovascularComponent implements OnInit {

  tamizaje: TamizajeModel = new TamizajeModel();
  today= new Date();
  fechaToday = "";
  constructor(private TamizajeService: TamizajeService, private route: ActivatedRoute) { this.fechaToday = formatDate(this.today, 'yyyy/MM/dd', 'en');}
  
  model: any;
  resultado = 0;
  normal_glicemia = 'normal';
  
  @ViewChild('instance', { static: true }) instance: NgbTypeahead;
  focus$ = new Subject<string>();
  click$ = new Subject<string>();

  search = (text$: Observable<string>) => {
    const debouncedText$ = text$.pipe(debounceTime(200), distinctUntilChanged());
    const clicksWithClosedPopup$ = this.click$.pipe(filter(() => !this.instance.isPopupOpen()));
    const inputFocus$ = this.focus$;

    return merge(debouncedText$, inputFocus$, clicksWithClosedPopup$).pipe(
      map(term => (term === '' ? states
        : states.filter(v => v.toLowerCase().indexOf(term.toLowerCase()) > -1)).slice(0, 10))
    );
  }

  searchEst = (text$: Observable<string>) => {
    const debouncedText$ = text$.pipe(debounceTime(200), distinctUntilChanged());
    const clicksWithClosedPopup$ = this.click$.pipe(filter(() => !this.instance.isPopupOpen()));
    const inputFocus$ = this.focus$;

    return merge(debouncedText$, inputFocus$, clicksWithClosedPopup$).pipe(
      map(term => (term === '' ? establecimiento
        : establecimiento.filter(v => v.toLowerCase().indexOf(term.toLowerCase()) > -1)).slice(0, 10))
    );
  }

  searchProfesional = (text$: Observable<string>) => {
    const debouncedText$ = text$.pipe(debounceTime(200), distinctUntilChanged());
    const clicksWithClosedPopup$ = this.click$.pipe(filter(() => !this.instance.isPopupOpen()));
    const inputFocus$ = this.focus$;

    return merge(debouncedText$, inputFocus$, clicksWithClosedPopup$).pipe(
      map(term => (term === '' ? profesional
        : profesional.filter(v => v.toLowerCase().indexOf(term.toLowerCase()) > -1)).slice(0, 10))
    );
  }
  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    const idd = "nuevo";
    if (idd !== 'nuevo') {
      console.log("ENTRA AQUI2");
      this.TamizajeService.getTamizaje(id)
        .subscribe((resp: TamizajeModel) => {
          console.log(resp);
          this.tamizaje = resp;
          this.tamizaje.id = id;
        });
    }

  }


  glicemia = [
    { id: "normal", label: "Normal" },
    { id: "anormal", label: "Anormal" }
  ]


  colesterol = [
    { id: "normal", label: "Normal" },
    { id: "anormal", label: "Anormal" }
  ]

  trigliceridos = [
    { id: "normal", label: "Normal" },
    { id: "anormal", label: "Anormal" }
  ]

  ta = [
    { id: "normal", label: "Normal" },
    { id: "anormal", label: "Anormal" }
  ]

  ekg = [
    { id: "normal", label: "Normal" },
    { id: "anormal", label: "Anormal" }
  ]


  calcularIMC(){
    this.resultado = this.tamizaje.peso / (this.tamizaje.talla * this.tamizaje.talla);
  }
  guardar(model: TamizajeModel, isValid: boolean) {
    
    swal.fire({
      title: 'Espere',
      text: 'Se esta guardando su informacion',
      type: 'info',
      allowOutsideClick: false
    });
    swal.showLoading();


    let peticion: Observable<any>;

    if (this.tamizaje.id) {
      peticion = this.TamizajeService.actualizarTamizaje(this.tamizaje);
    } else {
      peticion = this.TamizajeService.crearTamizaje(this.tamizaje);
    }

    peticion.subscribe(resp => {
      swal.fire({
        title: this.tamizaje.nombre,
        text: 'Su informacion se a guardado correctamente',
        type: 'success'
      });
    });
  }

}
