import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TamizajeCardiovascularComponent } from './tamizaje-cardiovascular.component';

describe('TamizajeCardiovascularComponent', () => {
  let component: TamizajeCardiovascularComponent;
  let fixture: ComponentFixture<TamizajeCardiovascularComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TamizajeCardiovascularComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TamizajeCardiovascularComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
