import { Component, OnInit } from '@angular/core';
import { PermisoModel, PermisoInterface } from '../../models/permiso.model';
import { NgForm } from '@angular/forms';
import { PermissionService } from '../../services/permission.service';
import { ActivatedRoute } from '@angular/router';
import {Location} from '@angular/common';

@Component({
  selector: 'app-permis',
  templateUrl: './permis.component.html',
  styleUrls: ['./permis.component.css']
})
export class PermisComponent implements OnInit {

  public permiso: PermisoModel = new PermisoModel();
  public id = '';

  constructor( private location: Location, private PermisoService: PermissionService, private route: ActivatedRoute ) {
    this.PermisoService.getPermisos().subscribe();
  }

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');

    if ( this.id !== 'nuevo') {
      this.PermisoService.traerPermiso( this.id ).subscribe(
        (permiso: PermisoInterface) => {
          this.permiso = permiso;
        }
      );
    }

  }

  public validacion( form: NgForm ): void {
    if ( form.invalid ) {
      console.log('formulario no valido');
      return;
    }
    if (this.id !== 'nuevo') {
      this.editar();
    } else {
      this.guardar();
    }
  }

  public guardar(): void {
    this.PermisoService.agregarPermiso( this.permiso ).then(
      () => {
      this.location.back();
      }
    );
  }

  public editar(): void {
    this.PermisoService.actualizarPermiso( this.permiso ).then(
      () => {
      this.location.back();
      }
    );
  }
}
