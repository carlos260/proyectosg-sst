import { Component, OnInit } from '@angular/core';
import { PermissionService } from '../../services/permission.service';
import { PermisoInterface } from '../../models/permiso.model';


@Component({
  selector: 'app-permiso',
  templateUrl: './permiso.component.html',
  styleUrls: ['./permiso.component.css']
})
export class PermisoComponent implements OnInit {

  public permisos: PermisoInterface[] = [];
  cargando = true;

  constructor( private ps: PermissionService ) {
  }

  ngOnInit() {
    this.cargando = true;
    this.ps.getPermisos().subscribe(
      (permisos) => {
        this.permisos = permisos;
        this.cargando = false;
      }
    );
  }

  public removerPermiso(permiso: PermisoInterface): void {
    this.ps.removerPermiso(permiso);
  }

}
