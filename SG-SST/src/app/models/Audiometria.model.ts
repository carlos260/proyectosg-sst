export class audiometria {

    id: string;
    fecha: Date;
    establecimiento: string;
    ciudad: string;
    nombres: string;
    cedula: string;
    edad: number;
    cargo: string;
    sexo: string;
    area: string;
    antiguedad: number;
    HipertensionArterial = "no";
    Diabetis = "no";
    tumoresSNC = "no";
    Acufenos = "no";
    Vertigo = "no";
    Otalgia = "no";
    Otorrea = "no";
    Otorragia = "no";
    Ruido = "no";
    ActualaRuido = "no";
    Toxicos = "no";
    Antituberculosis = "no";
    Aspirinas = "no";
    Antibioticos = "no";
    Fumador = "no";
    Alcoholico = "no";
    AuducionEstado = "no";
    Repeticion = "no";
    AumentoVolumen = "no";
    EscucharenRuido = "no";
    RuidosIntensos = "no";
    OtoscopiaDerecho = "normal";
    OtoscopiaIzquierdo = "normal";
    TimpanicaDerecho = "normal";
    TimpanicaIzquierda = "normal";
    audiomD = "normal";
    audiomI = "normal";
    result = "normal";
    entregaR = "si";
    ObjMembranaTimpanicaDerecho: string;
    ObjMembranaTimpanicaIzquierda: string;
    AudiometriaDerecho: string;
    AudiometriaIzquierdo: string;
    DiagnosticDes: string;
    ResultadoAudiometria: string;
    Recomendaciones: string;
    EntregaResultado: boolean;
    NombreProfesional: string;
}


