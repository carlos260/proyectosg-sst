
export interface EmpresaInterface{
    id: string,
    nombre: string,
    empresaVinculada: string
}

export class EmpresaModel{
    id: string;
    nombre: string;
    empresaVinculada: string;

    constructor(){
        this.empresaVinculada = "";
    }
}
