


export interface PermisoInterface {
   id: string;
   nombre: string;
   permiso: boolean;
}


export class PermisoModel {

   id: string;
   nombre: string;
   permiso: boolean;

   constructor( ) {
      this.permiso = false;
   }

}
