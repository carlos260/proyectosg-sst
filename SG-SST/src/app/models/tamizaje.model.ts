export class TamizajeModel{

    id: string;
    fecha: Date;
    establecimiento: string;
    ciudad: string;
    nombre: string;
    cedula: string;
    resultado_glicemia: number;
    resultado_colesterol: number;
    resultado_trigliceridos: number;
    res_glicemia = "normal";
    res_colesterol = "normal";
    res_trigliceridos = "normal";
    peso: number;
    talla: number;
    indice_masa: number;
    tension_arterial: number;
    resultado_ta = "normal";
    resultado_ekg = "normal";
    nombre_profesional: string;
 }