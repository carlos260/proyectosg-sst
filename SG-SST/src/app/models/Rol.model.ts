


export class RolModel {
    id: string;
    nombre: string;
    estado: boolean;
    permisos: any[];
    constructor() {
        this.estado = false;
    }
}

export interface RolInterface {
    id: string;
    nombre: string;
    estado: boolean;
    permisos: any[];
}
