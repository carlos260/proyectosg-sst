import { RolInterface } from './Rol.model';


export interface userInterface{
    id: string;
    nombre:string;
    apellido: string;
    cedula: string;
    fechaed: Date;
    email: string;
    rol: string;
    empresaAsociada: string;
    protoURL?: string;
    displayName?: string;
}

export class userModel{
    id: string;
    nombre:string;
    apellido: string;
    cedula: string;
    fechaed: Date;
    email: string;
    rol: string;
    empresaAsociada: string;
    protoURL?: string;
    displayName?: string;    
}
