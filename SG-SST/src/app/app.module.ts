import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { LoginComponent } from './components/login/login.component';
import { UserComponent } from './components/user/user.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { HomeComponent } from './components/home/home.component';
import { CardUserComponent } from './components/card-user/card-user.component';


import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PermisoComponent } from './components/permisos/permiso.component';
import { PermisComponent } from './components/permis/permis.component';
import { RolPermisoComponent } from './components/rol-permiso/rol-permiso.component';

import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { environment } from '../environments/environment';
import { RolesComponent } from './components/roles/roles.component';
import { RolComponent } from './components/rol/rol.component';
import { AnalistaComponent } from './components/analista/analista.component';
import { CrearReporteComponent } from './components/crear-reporte/crear-reporte.component';
import { EspirometriaComponent } from './components/forms/espirometria/espirometria.component';
import { ChartsModule } from 'ng2-charts';
import { TamizajeCardiovascularComponent } from './components/forms/tamizaje-cardiovascular/tamizaje-cardiovascular.component';
import { CondicionesSaludComponent } from './components/forms/condiciones-salud/condiciones-salud.component';
import { HitoriaMedicoOcupacionalComponent } from './components/forms/hitoria-medico-ocupacional/hitoria-medico-ocupacional.component';
import { EspirometriaService } from './services/espirometria.service';
import { AudiometriaOcupacionalComponent } from './components/forms/audiometria-ocupacional/audiometria-ocupacional.component';


import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { EmpresaComponent } from './components/empresa/empresa.component';
import { EmpresasComponent } from './components/empresas/empresas.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    LoginComponent,
    UserComponent,
    NavbarComponent,
    PermisoComponent,
    HomeComponent,
    PermisoComponent,
    PermisComponent,
    CardUserComponent,
    RolPermisoComponent,
    RolesComponent,
    RolComponent,
    AnalistaComponent,
    CrearReporteComponent,
    TamizajeCardiovascularComponent,
    EspirometriaComponent,
    CondicionesSaludComponent,
    HitoriaMedicoOcupacionalComponent,
    AudiometriaOcupacionalComponent,
    EmpresaComponent,
    EmpresasComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    AngularFireAuthModule,
    AngularFireStorageModule,
    ChartsModule,
    NgbModule,
  ],
  providers: [EspirometriaService],
  bootstrap: [AppComponent]
})
export class AppModule { }
