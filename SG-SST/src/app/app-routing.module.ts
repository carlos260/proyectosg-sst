import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserComponent } from './components/user/user.component';
import { LoginComponent } from './components/login/login.component';
import { PermisoComponent } from './components/permisos/permiso.component';
import { PermisComponent } from './components/permis/permis.component';
import { RolPermisoComponent } from './components/rol-permiso/rol-permiso.component';
import { RolesComponent } from './components/roles/roles.component';
import { RolComponent } from './components/rol/rol.component';
import { AnalistaComponent } from './components/analista/analista.component';
import { CrearReporteComponent } from './components/crear-reporte/crear-reporte.component';
import { EspirometriaComponent } from './components/forms/espirometria/espirometria.component';
import {TamizajeCardiovascularComponent} from './components/forms/tamizaje-cardiovascular/tamizaje-cardiovascular.component';
import { CondicionesSaludComponent } from './components/forms/condiciones-salud/condiciones-salud.component';
import { AudiometriaOcupacionalComponent } from './components/forms/audiometria-ocupacional/audiometria-ocupacional.component';
import { GuardService } from './services/guard.service';
import { HomeComponent } from './components/home/home.component';
import { EmpresaComponent } from './components/empresa/empresa.component';
import { EmpresasComponent } from './components/empresas/empresas.component';


const routes: Routes = [
  {path: 'User', component: UserComponent, canActivate: [GuardService]},
  {path: 'Empresa/:id', component: EmpresaComponent, canActivate:[GuardService]},
  {path: 'Empresas', component: EmpresasComponent, canActivate:[GuardService]},
  {path: 'Login', component: LoginComponent},
  {path: 'Home', component: HomeComponent, canActivate: [GuardService]},
  {path: 'espirometria', component: EspirometriaComponent, canActivate: [GuardService]},
  {path: 'permisos', component: PermisoComponent, canActivate: [GuardService]},
  {path: 'permiso/:id', component: PermisComponent, canActivate: [GuardService]},
  {path: 'rolPermiso', component: RolPermisoComponent, canActivate: [GuardService]},
  {path: 'roles', component: RolesComponent, canActivate: [GuardService]},
  {path: 'rol/:id', component: RolComponent, canActivate: [GuardService]},
  {path: 'analista', component: AnalistaComponent, canActivate: [GuardService]},
  {path: 'tamizaje-cardiovascular', component: TamizajeCardiovascularComponent , canActivate: [GuardService]},
  {path: 'tamizaje-cardiovascular/:id', component: TamizajeCardiovascularComponent, canActivate: [GuardService]},
  {path: 'condiciones-salud', component: CondicionesSaludComponent, canActivate: [GuardService]},
  {path: 'condiciones-salud/:id', component: CondicionesSaludComponent, canActivate: [GuardService]},
  {path: 'crear-reporte/:id', component: CrearReporteComponent, canActivate: [GuardService]},  
  {path: 'crear-reporte/:id', component: CrearReporteComponent, canActivate: [GuardService]},
  {path: 'analista', component: AnalistaComponent, canActivate: [GuardService]},
  {path: 'prueba', component: AudiometriaOcupacionalComponent, canActivate: [GuardService]},
  {path: 'audiometria-ocupacional', component: AudiometriaOcupacionalComponent , canActivate: [GuardService]},
  {path: 'audiometria-ocupacional/:id', component: AudiometriaOcupacionalComponent, canActivate: [GuardService]},
  {path: '**' , pathMatch: 'full', redirectTo: 'Login'},
  {path: '' , pathMatch: 'full', redirectTo: 'Login'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
